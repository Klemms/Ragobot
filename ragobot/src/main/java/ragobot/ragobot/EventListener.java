package ragobot.ragobot;

import ragobot.ragobot.rights.User;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageSendEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserSpeakingEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelMoveEvent;
import sx.blah.discord.handle.obj.IVoiceChannel;

public class EventListener {

	@EventSubscriber
    public void onReadyEvent(ReadyEvent event) {
		try {
			App.init();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	@EventSubscriber
    public void onVoiceEvent(UserSpeakingEvent event) {
		DiscordGuild server = App.getServerByGuildID(event.getGuild().getStringID());
		if(server != null) {
			User usr = server.getUserFromID(event.getUser().getID());
			
			if(usr != null) {
				if(event.isSpeaking()) {
					usr.setTimeStartedToTalk(System.currentTimeMillis());
				} else if(usr.getTimeStartedToTalk() > -1L) {
					System.out.println(usr.getMillisecondTalked() + " ** " + System.currentTimeMillis() + " ** " + usr.getTimeStartedToTalk());
					usr.setMillisecondTalked(usr.getMillisecondTalked() + (System.currentTimeMillis() - usr.getTimeStartedToTalk()));
					usr.setTimeStartedToTalk(-1L);
				}
			}
		}
    }
	
	@EventSubscriber
    public void onUserJoinVoiceChannel(UserVoiceChannelJoinEvent event) {
		for(IVoiceChannel channel : App.discord.getConnectedVoiceChannels()) {
			if(event.getVoiceChannel().getStringID().equals(channel.getStringID())) {
				DiscordGuild server = App.getServerByGuildID(event.getVoiceChannel().getGuild().getStringID());
				if(server != null) {
					try {
						server.playAudio("dingdong");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				return;
			}
		}
	}
	
	@EventSubscriber
    public void onUserMoveVoiceChannel(UserVoiceChannelMoveEvent event) {
		for(IVoiceChannel channel : App.discord.getConnectedVoiceChannels()) {
			if(event.getNewChannel().getStringID().equals(channel.getStringID()) && !event.getUser().getStringID().equals(App.discord.getOurUser().getStringID())) {
				DiscordGuild server = App.getServerByGuildID(event.getNewChannel().getGuild().getStringID());
				if(server != null) {
					try {
						server.playAudio("dingdong");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				return;
			}
		}
	}
	
	@EventSubscriber
    public void onUserLeaveVoiceChannel(UserVoiceChannelLeaveEvent event) {
		DiscordGuild server = App.getServerByGuildID(event.getGuild().getStringID());
		if(server != null) {
			User usr = server.getUserFromID(event.getUser().getID());
			
			if(usr != null) {
				if(usr.getTimeStartedToTalk() > -1L) {
					System.out.println(usr.getMillisecondTalked() + " ** " + System.currentTimeMillis() + " ** " + usr.getTimeStartedToTalk());
					usr.setMillisecondTalked(usr.getMillisecondTalked() + (System.currentTimeMillis() - usr.getTimeStartedToTalk()));
					usr.setTimeStartedToTalk(-1L);
				}
			}
		}
	}
	
	@EventSubscriber
    public void onUserJoinEvent(UserJoinEvent event) {
		DiscordGuild server = App.getServerByGuildID(event.getGuild().getStringID());
		if(server != null) {
			server.initUsers();
		}
	}
	
	@EventSubscriber
    public void onMessageByBot(MessageSendEvent event) {
		Message.messages.add(new Message(System.currentTimeMillis(), 45000, event.getMessage()));
	}

	@EventSubscriber
    public void onMessageByUser(MessageReceivedEvent event) {
		if(event.getMessage().getContent().startsWith("!")) {
			for(int a = 0; a < App.getCommand().size(); a++) {
				if(App.getCommand().get(a).getCommandName().toLowerCase().equals(event.getMessage().getContent().toLowerCase().split(" ")[0].substring(1, event.getMessage().getContent().toLowerCase().split(" ")[0].length()))) {
					DiscordGuild server = App.getServerByGuildID(event.getMessage().getGuild().getStringID());
					if(server != null) {
						if(server.getUserFromID(event.getMessage().getAuthor().getStringID()).hasPermission(App.getCommand().get(a).getPermission())) {
							try {
								App.getCommand().get(a).onCommand(server, event.getMessage());
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							App.newMessage(event.getMessage().getChannel(), "Désolé " + event.getMessage().getAuthor().getDisplayName(App.discord.getGuildByID(server.getLongGuildID())) + " mais tu n'as pas la permission :/");
						}
						System.out.println(event.getMessage().getAuthor().getName() + " executed command : " + event.getMessage().getContent());
					} else {
						App.newMessage(event.getMessage().getChannel(), "Le support de ce serveur n'est pas activé");
					}
				}
			}
			Message.messages.add(new Message(System.currentTimeMillis(), 10000, event.getMessage()));
		}
    }
}
