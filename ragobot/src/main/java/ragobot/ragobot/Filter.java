package ragobot.ragobot;

public enum Filter {

	ALL,
	CONNECTED,
	DISCONNECTED,
	IDLE,
	STREAMING,
	DO_NOT_DISTURB;
}
