package ragobot.ragobot;

public enum Descriptions {

	_0("pue"),
	_1("sent des pieds"),
	_2("a un serpent dans sa botte"),
	_5("se sent pas bien"),
	_6("fait dodo"),
	_7("est boosted"),
	_8("est salé"),
	_9("a ragequit");
	
	public String description;
	
	Descriptions(String description) {
		this.description = description;
	}
}
