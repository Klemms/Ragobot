package ragobot.ragobot.threads;

import ragobot.ragobot.Message;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class ThreadDeleteMessage extends Thread {
	
	public ThreadDeleteMessage() {
		this.setDaemon(true);
	}

	@Override
	public void run() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		while(true) {
			try {
				for(int a = 0; a < Message.messages.size(); a++) {
					if((Message.messages.get(a).timestamp + Message.messages.get(a).messageTime) < System.currentTimeMillis() && !Message.messages.get(a).deleted) {
						final int k = a;
						Message.messages.get(a).deleted = true;
						RequestBuffer.request(() -> {
							try {
								Message.messages.get(k).message.delete();
							} catch (MissingPermissionsException e) {
								e.printStackTrace();
							} catch (DiscordException e) {
								e.printStackTrace();
							}
					        return null;
					    });
					}
				}
				
				Thread.sleep(100);
			} catch (Exception e) {
				e.printStackTrace();
				new ThreadDeleteMessage();
			}
		}
	}
}
