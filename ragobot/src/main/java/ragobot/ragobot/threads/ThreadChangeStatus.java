package ragobot.ragobot.threads;

import java.util.concurrent.ThreadLocalRandom;

import ragobot.ragobot.App;
import ragobot.ragobot.Descriptions;
import ragobot.ragobot.Filter;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class ThreadChangeStatus extends Thread {
	
	public ThreadChangeStatus() {
		this.setDaemon(true);
	}

	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(60000);
				if(App.discord.isReady() && App.getUsers(Filter.CONNECTED).length > 0) {
					IUser[] users = App.getUsers(Filter.CONNECTED);
					
					int randomQuote = ThreadLocalRandom.current().nextInt(0, Descriptions.values().length);
					int randomUser = ThreadLocalRandom.current().nextInt(0, users.length);

					RequestBuffer.request(() -> {
						try {
							App.discord.online(users[randomUser].getName() + " " + Descriptions.values()[randomQuote].description);
						} catch (MissingPermissionsException e) {
							e.printStackTrace();
						} catch (DiscordException e) {
							e.printStackTrace();
						}
			            return null;
			        });
				}
			} catch (Exception e) {
				e.printStackTrace();
				new ThreadChangeStatus().start();
			}
		}
	}
}
