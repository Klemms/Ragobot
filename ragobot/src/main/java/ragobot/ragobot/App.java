package ragobot.ragobot;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.endpoints.static_data.constant.ItemListTags;
import net.rithms.riot.api.endpoints.static_data.constant.Locale;
import net.rithms.riot.api.endpoints.static_data.dto.Item;
import net.rithms.riot.api.endpoints.static_data.dto.ItemList;
import net.rithms.riot.constant.Platform;
import ragobot.ragobot.commands.Command;
import ragobot.ragobot.commands.CommandAddPermission;
import ragobot.ragobot.commands.CommandAh;
import ragobot.ragobot.commands.CommandBanQuote;
import ragobot.ragobot.commands.CommandBroadcast;
import ragobot.ragobot.commands.CommandClear;
import ragobot.ragobot.commands.CommandDaccord;
import ragobot.ragobot.commands.CommandHelp;
import ragobot.ragobot.commands.CommandItem;
import ragobot.ragobot.commands.CommandJoin;
import ragobot.ragobot.commands.CommandLeave;
import ragobot.ragobot.commands.CommandPermissions;
import ragobot.ragobot.commands.CommandPlay;
import ragobot.ragobot.commands.CommandQuote;
import ragobot.ragobot.commands.CommandQuoteFr;
import ragobot.ragobot.commands.CommandRandom;
import ragobot.ragobot.commands.CommandRandomItem;
import ragobot.ragobot.commands.CommandRemovePermission;
import ragobot.ragobot.commands.CommandShutdown;
import ragobot.ragobot.commands.CommandSoundList;
import ragobot.ragobot.commands.CommandStop;
import ragobot.ragobot.commands.CommandTG;
import ragobot.ragobot.commands.CommandTaric;
import ragobot.ragobot.commands.CommandUpdateSoundList;
import ragobot.ragobot.servlet.TimeTalkedServlet;
import ragobot.ragobot.threads.ThreadChangeStatus;
import ragobot.ragobot.threads.ThreadDeleteMessage;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;
import sx.blah.discord.util.RequestBuffer;

public class App {
	
	public static final String BOT_ID = "263509360276275200";
	
	public static final String DDRAGON_VERSION = "7.19.1";
	
	public static IDiscordClient discord;
	public static List<DiscordGuild> serverList = new ArrayList<DiscordGuild>();
	
	public static String RIOT_KEY = "RGAPI-763d9083-c8ab-4854-8fcc-5b55f11d55a9";
	public static Platform DDRAGON_REGION = Platform.EUW;
	
	public static RiotApi api;
	
	public static List<Item> itemList = new ArrayList<Item>();
	
	private static List<Command> commands = new ArrayList<Command>();
	
	public static boolean disableItems = false;
	
	public static Server webServer;
	
	public static void init() throws Exception {
		webServer = new Server(1337);
		
		FilterHolder filterHolder = new FilterHolder(CrossOriginFilter.class);
		filterHolder.setInitParameter("allowedOrigins", "*");
		filterHolder.setInitParameter("allowedMethods", "GET, POST");
		
		ServletContextHandler handler = new ServletContextHandler(webServer, "/");
		handler.addServlet(TimeTalkedServlet.class, "/");
		handler.addFilter(filterHolder, "/*", null);
		try {
			webServer.start();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		new ThreadChangeStatus().start();
		new ThreadDeleteMessage().start();

		App.registerCommand(new CommandClear());
		App.registerCommand(new CommandTG());
		App.registerCommand(new CommandBroadcast());
		App.registerCommand(new CommandPlay());
		App.registerCommand(new CommandQuote());
		App.registerCommand(new CommandQuoteFr());
		App.registerCommand(new CommandBanQuote());
		App.registerCommand(new CommandJoin());
		App.registerCommand(new CommandHelp());
		App.registerCommand(new CommandPermissions());
		App.registerCommand(new CommandRandom());
		App.registerCommand(new CommandRandomItem());
		App.registerCommand(new CommandStop());
		App.registerCommand(new CommandSoundList());
		App.registerCommand(new CommandUpdateSoundList());
		App.registerCommand(new CommandAddPermission());
		App.registerCommand(new CommandRemovePermission());
		App.registerCommand(new CommandTaric());
		App.registerCommand(new CommandLeave());
		App.registerCommand(new CommandShutdown());
		App.registerCommand(new CommandAh());
		App.registerCommand(new CommandDaccord());
		App.registerCommand(new CommandItem());
		
		for(int a = 0; a < App.discord.getConnectedVoiceChannels().size(); a++) {
			App.discord.getConnectedVoiceChannels().get(a).leave();
		}
		
		System.out.println("Riot Items are " + (App.disableItems ? "disabled" : "enabled"));
		
		if(!App.disableItems) {
			try {
				ApiConfig apiConfig = new ApiConfig();
				apiConfig.setKey(RIOT_KEY);
				apiConfig.setDebugLevel(Level.ALL);
				
				api = new RiotApi(apiConfig);
				
				ItemList rritemList = api.getDataItemList(DDRAGON_REGION, Locale.EN_US, App.DDRAGON_VERSION, ItemListTags.ALL);
				
				Collection<Item> rItemList = rritemList.getData().values();
				
				System.out.println("Items : " + rItemList.size());
				
				for(Item item : rItemList) {
					if(item.getName() != null) {
						itemList.add(item);
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if(!Files.exists(Paths.get(System.getProperty("user.dir"), "cache"))) {
			Files.createDirectory(Paths.get(System.getProperty("user.dir"), "cache"));
		}
		
		App.serverList.add(new DiscordGuild("177501943885725696", "177501943885725696"));
		App.serverList.add(new DiscordGuild("216233281660649472", "216233281660649472"));
		App.serverList.add(new DiscordGuild("257655603248234499", "257655603248234499"));
	}
	
    public static void main(String[] args) {
		if (args.length > 0) {
			for(int a = 0; a < args.length; a++) {
				if(args[a].substring(0, 1).equals("-")) {
					String argument = args[a].substring(1, args[a].length() - 1).split("=")[0];
					String[] values = args[a].split("=");
					String value = "";
					for(int k = 1; k < values.length; k++) {
						value += values[k];
						if(k < values.length - 1) {
							value += "=";
						}
					}
					System.out.println("Arg #" + a + " : " + argument + " = " + value);
					if(argument.equals("riotkey")) {
						RIOT_KEY = value;
					} else if(argument.equals("ddragonregion")) {
						try {
							DDRAGON_REGION = Platform.valueOf(value);
						} catch(Exception e) {
							DDRAGON_REGION = Platform.EUW;
							e.printStackTrace();
						}
					} else if(argument.equals("disableitems")) {
						disableItems = Boolean.valueOf(value);
					}
				}
			}
		}
		
    	try {
    		discord = App.getClient("MjYzNTA5MzYwMjc2Mjc1MjAw.C0TDug.9ZVtLHKpMOMW_K7M4YxFCwSi304", true);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	EventDispatcher dispatcher = discord.getDispatcher();
    	dispatcher.registerListener(new EventListener());
    }
    
    public static void newMessage(IChannel channel, String message) {
		RequestBuffer.request(() -> {
			try {
				channel.sendMessage(message);
			} catch (RateLimitException e) {
				e.printStackTrace();
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
            return null;
        });
    }
    
    public static void newMessage(IChannel channel, String message, boolean useTTS) {
		RequestBuffer.request(() -> {
			try {
				channel.sendMessage(message, useTTS);
			} catch (RateLimitException e) {
				e.printStackTrace();
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
            return null;
        });
    }
    
    public static IDiscordClient getClient(String token, boolean login) throws Exception {
    	ClientBuilder clientBuilder = new ClientBuilder();
    	clientBuilder.withToken(token);
    	if(login) {
    		return clientBuilder.login();
    	} else {
    		return clientBuilder.build();
    	}
    }
    
    public static DiscordGuild getServerByGuildID(String guildID) {
    	for(DiscordGuild server : App.serverList) {
    		if(guildID.equals(server.getStringGuildID())) {
    			return server;
    		}
    	}
    	return null;
    }
    
    public static IUser[] getUsers(Filter filter) {
    	IUser[] arrayUsers = App.discord.getUsers().toArray(new IUser[App.discord.getUsers().size()]);
    	List<IUser> listUsers = new ArrayList<IUser>();
    	switch(filter) {
			case CONNECTED:
				for(IUser user : arrayUsers) {
					if(user.getPresence().getStatus() == StatusType.ONLINE) {
						listUsers.add(user);
					}
				}
				return listUsers.toArray(new IUser[listUsers.size()]);
			case DISCONNECTED:
				for(IUser user : arrayUsers) {
					if(user.getPresence().getStatus() == StatusType.OFFLINE) {
						listUsers.add(user);
					}
				}
				return listUsers.toArray(new IUser[listUsers.size()]);
			case IDLE:
				for(IUser user : arrayUsers) {
					if(user.getPresence().getStatus() == StatusType.IDLE) {
						listUsers.add(user);
					}
				}
				return listUsers.toArray(new IUser[listUsers.size()]);
			case STREAMING:
				for(IUser user : arrayUsers) {
					if(user.getPresence().getStatus() == StatusType.STREAMING) {
						listUsers.add(user);
					}
				}
				return listUsers.toArray(new IUser[listUsers.size()]);
			case DO_NOT_DISTURB:
				for(IUser user : arrayUsers) {
					if(user.getPresence().getStatus() == StatusType.DND) {
						listUsers.add(user);
					}
				}
				return listUsers.toArray(new IUser[listUsers.size()]);
			default:
	    		return arrayUsers;
    	}
    }
    
    /**
     * 
     * @param command The Command interface implementation of the Command to add
     */
    public static void registerCommand(Command command) {
    	App.commands.add(command);
    }
    
    /**
     * 
     * @return Non-Thread Safe List of Commands
     */
    public static List<Command> getCommand() {
    	return App.commands;
    }
}
