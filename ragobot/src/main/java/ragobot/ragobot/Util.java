package ragobot.ragobot;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Util {

	public static List<Path> getFilesInPath(Path path) throws Exception {
		if(!Files.exists(path)) {
			Files.createDirectories(path);
		}
		List<Path> files = new ArrayList<Path>();
		try(DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
			for(Path apath : directoryStream) {
				files.add(apath);
			}
		}
		
		return files;
	}
}
