package ragobot.ragobot;

import java.util.ArrayList;
import java.util.List;

import sx.blah.discord.handle.obj.IMessage;

public class Message {
	
	public static List<Message> messages = new ArrayList<Message>();

	public long timestamp;
	public long messageTime;
	public boolean deleted;
	public IMessage message;
	
	public Message(long timestamp, long messageTime, IMessage message) {
		this.timestamp = timestamp;
		this.messageTime = messageTime;
		this.deleted = false;
		this.message = message;
	}
}
