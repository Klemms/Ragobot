package ragobot.ragobot;

import java.io.File;
import java.nio.file.Path;

import org.apache.commons.io.FilenameUtils;

public class Sound {
	
	public Path soundPath;
	
	public Sound(Path soundPath) {
		this.soundPath = soundPath;
	}
	
	public String getSoundName() {
		return FilenameUtils.removeExtension(this.soundPath.getFileName().toString()).toLowerCase();
	}
	
	public File getFile() {
		return this.soundPath.toFile();
	}
}
