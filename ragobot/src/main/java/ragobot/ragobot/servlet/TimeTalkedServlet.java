package ragobot.ragobot.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.User;
import sx.blah.discord.handle.obj.IUser;

public class TimeTalkedServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static Gson gson = new Gson();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String guild = req.getParameter("guild");
		
		if(guild == null) {
			resp.getWriter().println("Missing 'guild' parameter");
			return;
		}
		
		DiscordGuild discordGuild = App.getServerByGuildID(guild);
		
		if(discordGuild == null) {
			resp.getWriter().println("Guild isn't supported by this bot");
			return;
		}
		
		JsonArray array = new JsonArray();
		for(User user : discordGuild.getUsers()) {
			JsonObject userElement = new JsonObject();
			IUser iuser = user.getDiscordUser();
			
			if(iuser != null) {
				userElement.addProperty("discordName", iuser.getName());
				userElement.addProperty("guildNickName", iuser.getNicknameForGuild(discordGuild.getIGuild()));
				userElement.addProperty("avatarURL", iuser.getAvatarURL());
				userElement.addProperty("discordID", user.getUserID());
				userElement.addProperty("isBot", iuser.isBot());
				userElement.addProperty("timeTalked", user.getMillisecondTalked());
				
				array.add(userElement);
			}
		}
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("text/json; charset=utf-8");
		resp.getWriter().println(gson.toJson(array));
	}
}
