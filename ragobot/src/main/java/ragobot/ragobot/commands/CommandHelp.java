package ragobot.ragobot.commands;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandHelp implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String commands = "";
		for(int a = 0; a < App.getCommand().size(); a++) {
			if(server.getUserFromID(message.getAuthor().getStringID()).hasPermission(App.getCommand().get(a).getPermission())) {
				commands += App.getCommand().get(a).getCommandName() + ", ";
			}
		}
		final String cmds = commands;
		RequestBuffer.request(() -> {
			try {
				message.getChannel().sendMessage(message.getAuthor().getDisplayName(App.discord.getGuildByID(server.getLongGuildID())) + " tes commandes disponibles sont : " + cmds);
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
	        return null;
	    });
	}

	public String getCommandName() {
		return "help";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
