package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandBroadcast implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		String text = "";
		if(messages.length > 1) {
			for(int a = 1; a < messages.length; a++) {
				text += messages[a] + " ";
			}
			server.broadcastToAllChannels(text, true);
		}
	}

	public String getCommandName() {
		return "broadcast";
	}

	public Permission getPermission() {
		return Permission.COMMAND_ADVANCED;
	}
}
