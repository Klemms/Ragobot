package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandJoin implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		if(message.getAuthor().getVoiceStateForGuild(message.getGuild()).getChannel() != null) {
			message.getAuthor().getVoiceStateForGuild(message.getGuild()).getChannel().join();
		}
	}

	public String getCommandName() {
		return "join";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
