package ragobot.ragobot.commands;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IVoiceChannel;

public class CommandLeave implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		for(IVoiceChannel voiceChannel : App.discord.getConnectedVoiceChannels()) {
			if(voiceChannel.getGuild().getStringID().equals(message.getGuild().getStringID())) {
				voiceChannel.leave();
				break;
			}
		}
	}

	public String getCommandName() {
		return "leave";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
