package ragobot.ragobot.commands;

import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.math.NumberUtils;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandRandom implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		if(messages.length == 3) {
			if(NumberUtils.isCreatable(messages[1]) && NumberUtils.isCreatable(messages[2])) {
				int random = ThreadLocalRandom.current().nextInt(NumberUtils.createInteger(messages[1]), NumberUtils.createInteger(messages[2]) + 1);
				RequestBuffer.request(() -> {
					try {
						message.getChannel().sendMessage("Roulement de tambour... Et c'est le " + random + " [" + NumberUtils.createInteger(messages[1]) + ";" + NumberUtils.createInteger(messages[2]) + "]");
					} catch (MissingPermissionsException e) {
						e.printStackTrace();
					} catch (DiscordException e) {
						e.printStackTrace();
					}
			        return null;
			    });
			}
		}
	}

	public String getCommandName() {
		return "random";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
