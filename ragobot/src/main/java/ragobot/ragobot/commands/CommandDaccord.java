package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandDaccord implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		server.playAudio("daccord");
	}

	public String getCommandName() {
		return "ok";
	}

	public Permission getPermission() {
		return Permission.PLAY_SOUND;
	}
}
