package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandTG implements Command {
	
	public int audioToPlay = 0;

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		switch(audioToPlay) {
			case 0:
				server.playAudio("tg");
				break;
			case 1:
				server.playAudio("taisezvous");
				break;
			case 2:
				server.playAudio("chut");
				audioToPlay = -1;
				break;
		}
		audioToPlay++;
	}

	public String getCommandName() {
		return "tg";
	}

	public Permission getPermission() {
		return Permission.PLAY_SOUND;
	}
}
