package ragobot.ragobot.commands;

import java.util.concurrent.ThreadLocalRandom;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.Sound;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandBanQuote implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		if(messages.length == 2) {
			Sound quote = server.getBanQuoteFromChampionName(messages[1]);
			if(quote != null) {
				server.playAudio(quote);
				return;
			}
			App.newMessage(message.getChannel(), "Ce champion n'existe pas");
			return;
		} else if(messages.length == 1) {
			int random = ThreadLocalRandom.current().nextInt(0, server.banQuotes.size());
			server.playAudio(server.banQuotes.get(random));
			return;
		}
		App.newMessage(message.getChannel(), "La commande est : !quote <champion name>");
	}

	public String getCommandName() {
		return "banquote";
	}

	public Permission getPermission() {
		return Permission.PLAY_SOUND;
	}
}
