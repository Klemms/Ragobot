package ragobot.ragobot.commands;

import org.apache.commons.lang3.math.NumberUtils;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandClear implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		if(messages.length == 2) {
			if(NumberUtils.isCreatable(messages[1])) {
				int number = NumberUtils.createInteger(messages[1]);
				if(number >= 2 && number <= 50) {
					message.getChannel().getMessageHistory(number).bulkDelete();
				} else {
					App.newMessage(message.getChannel(), "Number must be between : [2;50]");
				}
			}
		}
	}

	public String getCommandName() {
		return "clear";
	}

	public Permission getPermission() {
		return Permission.PLAY_SOUND;
	}
}
