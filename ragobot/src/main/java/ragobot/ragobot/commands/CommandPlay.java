package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandPlay implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		if(messages.length == 2) {
			for(int a = 0; a < server.sounds.size(); a++) {
				if(server.sounds.get(a).getSoundName().equals(messages[1].toLowerCase())) {
					server.playAudio(server.sounds.get(a));
					return;
				}
			}
		}
	}

	public String getCommandName() {
		return "play";
	}

	public Permission getPermission() {
		return Permission.PLAY_SOUND;
	}
}
