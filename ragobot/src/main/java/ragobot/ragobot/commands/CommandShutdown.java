package ragobot.ragobot.commands;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandShutdown implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		App.discord.logout();
		for(DiscordGuild guild : App.serverList) {
			guild.writeUsers();
		}
		Thread.sleep(2000);
		System.exit(0);
	}

	public String getCommandName() {
		return "shutdown";
	}

	public Permission getPermission() {
		return Permission.ADMIN;
	}
}
