package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandTaric implements Command {
	
	public int audioToPlay = 0;

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		switch(audioToPlay) {
			case 0:
				server.playAudio("taric1");
				break;
			case 1:
				server.playAudio("taric2");
				break;
			case 2:
				server.playAudio("taric3");
				break;
			case 3:
				server.playAudio("taric4");
				audioToPlay = -1;
				break;
		}
		audioToPlay++;
	}

	public String getCommandName() {
		return "taric";
	}

	public Permission getPermission() {
		return Permission.PLAY_SOUND;
	}
}
