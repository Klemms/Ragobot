package ragobot.ragobot.commands;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandPermissions implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String permissions = "";
		for(int a = 0; a < server.getUserFromID(message.getAuthor().getStringID()).getPermissions().size(); a++) {
			permissions += server.getUserFromID(message.getAuthor().getStringID()).getPermissions().get(a).toString() + ", ";
		}
		final String prms = permissions;
		RequestBuffer.request(() -> {
			try {
				message.getChannel().sendMessage(message.getAuthor().getDisplayName(App.discord.getGuildByID(server.getLongGuildID())) + " tes permissions sont : " + prms);
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
	        return null;
	    });
	}

	public String getCommandName() {
		return "permissions";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
