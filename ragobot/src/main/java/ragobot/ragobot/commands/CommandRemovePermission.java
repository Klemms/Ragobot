package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import ragobot.ragobot.rights.User;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandRemovePermission implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		if(messages.length == 3) {
			Permission permission = Permission.valueOf(messages[2]);
			User user = server.getUserFromID(messages[1]);
			if(user != null) {
				if(permission != null) {
					user.removePermission(permission);
					return;
				}
				RequestBuffer.request(() -> {
					try {
						message.getChannel().sendMessage("Cette permission (" + messages[2] + ") n'existe pas :/");
					} catch (MissingPermissionsException e) {
						e.printStackTrace();
					} catch (DiscordException e) {
						e.printStackTrace();
					}
			        return null;
			    });
			}
			RequestBuffer.request(() -> {
				try {
					message.getChannel().sendMessage("Cet utilisateur (" + messages[1] + ") n'existe pas :/");
				} catch (MissingPermissionsException e) {
					e.printStackTrace();
				} catch (DiscordException e) {
					e.printStackTrace();
				}
		        return null;
		    });
		}
		RequestBuffer.request(() -> {
			try {
				message.getChannel().sendMessage("La commande est : !removepermission <username> <permission>");
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
	        return null;
	    });
	}

	public String getCommandName() {
		return "removepermission";
	}

	public Permission getPermission() {
		return Permission.MODERATOR;
	}
}
