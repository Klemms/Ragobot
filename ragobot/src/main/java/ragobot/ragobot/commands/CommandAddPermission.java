package ragobot.ragobot.commands;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import ragobot.ragobot.rights.User;
import sx.blah.discord.handle.obj.IMessage;

public class CommandAddPermission implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		if(messages.length == 3) {
			Permission permission = Permission.valueOf(messages[2]);
			User user = server.getUserFromID(messages[1]);
			if(user != null) {
				if(permission != null) {
					user.addPermission(permission);
					return;
				}
				App.newMessage(message.getChannel(), "Cette permission (" + messages[2] + ") n'existe pas :/");
			}
			App.newMessage(message.getChannel(), "Cet utilisateur (" + messages[1] + ") n'existe pas :/");
		}
		App.newMessage(message.getChannel(), "La commande est : !addpermission <username> <permission>");
	}

	public String getCommandName() {
		return "addpermission";
	}

	public Permission getPermission() {
		return Permission.MODERATOR;
	}
}
