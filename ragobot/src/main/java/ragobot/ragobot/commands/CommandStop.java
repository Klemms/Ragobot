package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandStop implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		server.getPlayer().clear();
	}

	public String getCommandName() {
		return "stop";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
