package ragobot.ragobot.commands;

import java.util.Comparator;
import java.util.List;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.Sound;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandSoundList implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		List<Sound> soundList = server.sounds;
		soundList.sort(new Comparator<Sound>() {
			@Override
			public int compare(Sound o1, Sound o2) {
				int order = o1.getSoundName().toLowerCase().compareTo(o2.getSoundName().toLowerCase());
				if(order == 0) {
					return 0;
				} else if(order < 0) {
					return -1;
				} else if(order > 0) {
					return 1;
				}
				return 0;
			}
		});
		String sounds = "";
		for(int a = 0; a < soundList.size(); a++) {
			sounds += soundList.get(a).getSoundName() + " - ";
		}
		sounds += "";
		final String snds = sounds;
		RequestBuffer.request(() -> {
			try {
				new MessageBuilder(App.discord).withChannel(message.getChannel())
				.appendContent("Sounds (" + soundList.size() + ") :")
				.appendCode("", snds)
				.send();
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
	        return null;
	    });
	}
	
	public String getCommandName() {
		return "soundlist";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
