package ragobot.ragobot.commands;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.io.FileUtils;

import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandRandomItem implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		int random = ThreadLocalRandom.current().nextInt(0, App.itemList.size());
		if(!Files.exists(Paths.get(System.getProperty("user.dir"), "cache", App.itemList.get(random).getId() + ".png"))) {
			FileUtils.copyURLToFile(new URL("http://ddragon.leagueoflegends.com/cdn/" + App.DDRAGON_VERSION + "/img/item/" + App.itemList.get(random).getId() + ".png"), Paths.get(System.getProperty("user.dir"), "cache", App.itemList.get(random).getId() + ".png").toFile());
		}
		RequestBuffer.request(() -> {
			try {
				message.getChannel().sendFile(App.itemList.get(random).getName() + " - " + App.itemList.get(random).getGold().getTotal() + "g - " + App.itemList.get(random).getPlaintext(), Paths.get(System.getProperty("user.dir"), "cache", App.itemList.get(random).getId() + ".png").toFile());
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
            return null;
        });
	}

	public String getCommandName() {
		return "randomitem";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
