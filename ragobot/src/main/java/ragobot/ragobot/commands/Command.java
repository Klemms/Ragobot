package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public interface Command {
	
	/**
	 * 
	 * @param channel The IChannel where the command was executed
	 * @param user The IUser who executed the command
	 */
	public void onCommand(DiscordGuild server, IMessage message) throws Exception;

	/**
	 * 
	 * @return The text after the !
	 */
	public String getCommandName();
	
	public Permission getPermission();
}
