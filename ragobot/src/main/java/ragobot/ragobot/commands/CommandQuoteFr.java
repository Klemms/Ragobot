package ragobot.ragobot.commands;

import java.util.concurrent.ThreadLocalRandom;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.Sound;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandQuoteFr implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String[] messages = message.getContent().split(" ");
		if(messages.length == 2) {
			Sound quote = server.getQuoteFrFromChampionName(messages[1]);
			if(quote != null) {
				server.playAudio(quote);
				return;
			}
			RequestBuffer.request(() -> {
				try {
					message.getChannel().sendMessage("Ce champion n'existe pas");
				} catch (MissingPermissionsException e) {
					e.printStackTrace();
				} catch (DiscordException e) {
					e.printStackTrace();
				}
		        return null;
		    });
			return;
		} else if(messages.length == 1) {
			int random = ThreadLocalRandom.current().nextInt(0, server.quotesFr.size());
			server.playAudio(server.quotesFr.get(random));
			return;
		}
		RequestBuffer.request(() -> {
			try {
				message.getChannel().sendMessage("La commande est : !quotefr <champion name>");
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
	        return null;
	    });
	}

	public String getCommandName() {
		return "quotefr";
	}

	public Permission getPermission() {
		return Permission.PLAY_SOUND;
	}
}
