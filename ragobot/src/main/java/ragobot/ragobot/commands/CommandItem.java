package ragobot.ragobot.commands;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;

import net.rithms.riot.api.endpoints.static_data.dto.Item;
import ragobot.ragobot.App;
import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandItem implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		String msg = message.getContent().replace("!item", "").replace(" ", "");
		if(!msg.equals(null) && !msg.equals(" ")) {
			for(Item item : App.itemList) {
				if(item.getName().replace(" ", "").toLowerCase().contains(msg.toLowerCase())) {
					if(!Files.exists(Paths.get(System.getProperty("user.dir"), "cache", item.getId() + ".png"))) {
						FileUtils.copyURLToFile(new URL("http://ddragon.leagueoflegends.com/cdn/" + App.DDRAGON_VERSION + "/img/item/" + item.getId() + ".png"), Paths.get(System.getProperty("user.dir"), "cache", item.getId() + ".png").toFile());
					}
					RequestBuffer.request(() -> {
						try {
							message.getChannel().sendFile(item.getName() + " - " + item.getGold().getTotal() + "g - " + item.getPlaintext(), Paths.get(System.getProperty("user.dir"), "cache", item.getId() + ".png").toFile());
						} catch (MissingPermissionsException e) {
							e.printStackTrace();
						} catch (DiscordException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
			            return null;
			        });
				}
			}
		}
	}

	public String getCommandName() {
		return "item";
	}

	public Permission getPermission() {
		return Permission.COMMAND_BASE;
	}
}
