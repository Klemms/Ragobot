package ragobot.ragobot.commands;

import ragobot.ragobot.DiscordGuild;
import ragobot.ragobot.rights.Permission;
import sx.blah.discord.handle.obj.IMessage;

public class CommandUpdateSoundList implements Command {

	public void onCommand(DiscordGuild server, IMessage message) throws Exception {
		server.reloadSounds();
	}

	public String getCommandName() {
		return "reload";
	}

	public Permission getPermission() {
		return Permission.COMMAND_ADVANCED;
	}
}
