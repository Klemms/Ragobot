package ragobot.ragobot;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import ragobot.ragobot.rights.Permission;
import ragobot.ragobot.rights.User;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.audio.AudioPlayer;
import sx.blah.discord.util.audio.AudioPlayer.Track;

public class DiscordGuild {
	
	private String stringGuildID;
	private long guildID;
	private String mainChannelID;
	private AudioPlayer player;
	private List<User> users;
	
	public List<Sound> sounds;
	public List<Sound> quotes;
	public List<Sound> quotesFr;
	public List<Sound> banQuotes;

	public DiscordGuild(String stringGuildID, String mainChannelID) {
		this.guildID = App.discord.getGuildByID(stringGuildID).getLongID();
		this.stringGuildID = stringGuildID;
		this.mainChannelID = mainChannelID;
		this.player = AudioPlayer.getAudioPlayerForGuild(App.discord.getGuildByID(this.getLongGuildID()));
		this.users = new ArrayList<User>();
		
		if(!Files.exists(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID()))) {
			try {
				Files.createDirectories(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(!Files.exists(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "recordings"))) {
			try {
				Files.createDirectories(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "recordings"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		this.sounds = new ArrayList<Sound>();
		this.quotes = new ArrayList<Sound>();
		this.quotesFr = new ArrayList<Sound>();
		this.banQuotes = new ArrayList<Sound>();

		this.initUsers();
		this.reloadSounds();
	}
	
	public void clearSounds() {
		this.sounds.clear();
		this.quotes.clear();
		this.quotesFr.clear();
		this.banQuotes.clear();
	}
	
	public void reloadSounds() {
		this.clearSounds();
		try {
			List<Path> soundsPaths = Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", "default", "sounds"));
			soundsPaths.addAll(Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "sounds")));
			for(int a = 0; a < soundsPaths.size(); a++) {
				this.sounds.add(new Sound(soundsPaths.get(a)));
			}
			
			List<Path> quotesPath = Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", "default", "quotes", "en"));
			quotesPath.addAll(Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "quotes", "en")));
			for(int a = 0; a < quotesPath.size(); a++) {
				this.quotes.add(new Sound(quotesPath.get(a)));
			}
			
			List<Path> quotesFrPath = Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", "default", "quotes", "fr"));
			quotesFrPath.addAll(Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "quotes", "fr")));
			for(int a = 0; a < quotesFrPath.size(); a++) {
				this.quotesFr.add(new Sound(quotesFrPath.get(a)));
			}
			
			List<Path> banQuotesPath = Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", "default", "banquotes"));
			banQuotesPath.addAll(Util.getFilesInPath(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "banquotes")));
			for(int a = 0; a < banQuotesPath.size(); a++) {
				this.banQuotes.add(new Sound(banQuotesPath.get(a)));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void broadcastToAllChannels(String message, boolean useTTS) {
		List<IChannel> channels = App.discord.getGuildByID(this.getLongGuildID()).getChannels();
		for(int a = 0; a < channels.size(); a++) {
			int k = a;
			App.newMessage(channels.get(k), message, useTTS);
		}
	}
	
	public User getUserFromUsername(String username) {
		for(int a = 0; a < this.users.size(); a++) {
			if(this.users.get(a).getUsername().equals(username)) {
				return this.users.get(a);
			}
		}
		return null;
	}
	
	public User getUserFromID(String userId) {
		for(int a = 0; a < this.users.size(); a++) {
			if(this.users.get(a).getUserID().equals(userId)) {
				return this.users.get(a);
			}
		}
		return null;
	}
	
	public void initUsers() {
		try {
			if(Files.exists(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "users.json"))) {
				try(Reader reader = new FileReader(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "users.json").toFile())) {
					Type userListType = new TypeToken<ArrayList<User>>(){}.getType();
					Gson gson = new GsonBuilder().create();
					List<User> userList = gson.fromJson(reader, userListType);
					this.users = userList;
				}
			}
			List<IUser> userArray = App.discord.getGuildByID(this.getLongGuildID()).getUsers();
			for(int a = 0; a < userArray.size(); a++) {
				if(this.getUserFromID(userArray.get(a).getID()) == null) {
					List<Permission> permissions = new ArrayList<Permission>();
					permissions.add(Permission.COMMAND_BASE);
					permissions.add(Permission.PLAY_SOUND);
					this.users.add(new User(userArray.get(a), permissions));
				}
			}
			writeUsers();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void writeUsers() throws IOException {
		try(Writer writer = new FileWriter(Paths.get(System.getProperty("user.dir"), "servers", this.getStringGuildID(), "users.json").toFile())) {
			Gson gson = new GsonBuilder().create();
			gson.toJson(this.users, writer);
		}
	}
    
    /**
     * 
     * @param soundName The name of the Sound
     * @return true if the sound exists, false if it doesn't
     * @throws Exception if any exception is thrown
     */
    public Track playAudio(String soundName) throws Exception {
    	Sound sound = this.getSoundFromName(soundName);
    	if(sound != null) {
        	return this.playAudio(sound);
    	}
    	return null;
    }
    
    /**
     * 
     * @param sound The Sound to play
     * @throws Exception if any exception is thrown
     */
    public Track playAudio(Sound sound) throws Exception {
    	System.out.println("Server : " + App.discord.getGuildByID(this.getLongGuildID()).getName() + " - Playing : " + sound.getSoundName());
    	return this.getPlayer().queue(sound.getFile());
    }
	
	public Sound getSoundFromName(String soundName) {
		for(int a = 0; a < this.sounds.size(); a++) {
			if(this.sounds.get(a).getSoundName().equals(soundName.toLowerCase())) {
				return this.sounds.get(a);
			}
		}
		return null;
	}
	
	public Sound getQuoteFromChampionName(String championName) {
		for(int a = 0; a < this.quotes.size(); a++) {
			if(this.quotes.get(a).getSoundName().equals(championName.toLowerCase())) {
				return this.quotes.get(a);
			}
		}
		return null;
	}
	
	public Sound getQuoteFrFromChampionName(String championName) {
		for(int a = 0; a < this.quotesFr.size(); a++) {
			if(this.quotesFr.get(a).getSoundName().equals(championName.toLowerCase())) {
				return this.quotesFr.get(a);
			}
		}
		return null;
	}
	
	public Sound getBanQuoteFromChampionName(String championName) {
		for(int a = 0; a < this.banQuotes.size(); a++) {
			if(this.banQuotes.get(a).getSoundName().equals(championName.toLowerCase())) {
				return this.banQuotes.get(a);
			}
		}
		return null;
	}
	
	public IGuild getIGuild() {
		return App.discord.getGuildByID(this.getLongGuildID());
	}

	public long getLongGuildID() {
		return guildID;
	}

	public String getStringGuildID() {
		return stringGuildID;
	}

	public String getMainChannelID() {
		return mainChannelID;
	}

	public AudioPlayer getPlayer() {
		return player;
	}

	public List<User> getUsers() {
		return users;
	}
}
