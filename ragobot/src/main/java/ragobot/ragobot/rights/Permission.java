package ragobot.ragobot.rights;

public enum Permission {

	ADMIN,
	MODERATOR,
	COMMAND_BASE,
	COMMAND_ADVANCED,
	PLAY_SOUND;
	
}