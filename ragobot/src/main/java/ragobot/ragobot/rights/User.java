package ragobot.ragobot.rights;

import java.util.List;

import ragobot.ragobot.App;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

public class User {

	// The username is here for easier identification in users.json
	@SuppressWarnings("unused")
	private String username;
	private String userID;
	private long millisecondTalked = 0L;
	private List<Permission> permissions;
	private transient long timeStartedToTalk = -1L;

	public User(IUser user, List<Permission> permissions) {
		this.userID = user.getStringID();
		this.username = user.getName();
		this.permissions = permissions;
		this.millisecondTalked = 0L;
		this.timeStartedToTalk = -1L;
	}
	
	public String getUserID() {
		return this.userID;
	}
	
	public String getStoredUsername() {
		return username;
	}
	
	public String getUsername() {
		return App.discord.getUserByID(this.userID).getName();
	}
	
	public IUser getDiscordUser() {
		return App.discord.getUserByID(this.getUserID());
	}
	
	public String getUserDisplayName(IGuild guild) {
		try {
			return App.discord.getUserByID(this.userID).getDisplayName(guild);
		} catch(Exception e) {
			return null;
		}
	}
	
	public List<Permission> getPermissions() {
		return this.permissions;
	}
	
	public boolean hasPermission(Permission permission) {
		if(this.permissions.contains(Permission.ADMIN)) {
			return true;
		} else {
			return this.permissions.contains(permission);
		}
	}
	
	public User addPermission(Permission permission) {
		if(!this.permissions.contains(permission)) {
			this.permissions.add(permission);
		}
		return this;
	}
	
	public User removePermission(Permission permission) {
		this.permissions.remove(permission);
		return this;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getMillisecondTalked() {
		return millisecondTalked;
	}

	public void setMillisecondTalked(long millisecondTalked) {
		this.millisecondTalked = millisecondTalked;
	}

	public long getTimeStartedToTalk() {
		return timeStartedToTalk;
	}

	public void setTimeStartedToTalk(long timeStartedToTalk) {
		this.timeStartedToTalk = timeStartedToTalk;
	}
}
